package main

import (
	"fmt"
	"strings"
)

// Function to find the longest common prefix
func longestCommonPrefix(strs []string) string {
	//tc: O(n*m)
	//sc: O(1)
	//horizon scan
	if len(strs) == 0 {
		return ""
	}
	//first item is a prefix
	prefix := strs[0]
	//loop all item in list
	for i := 1; i < len(strs); i++ {
		//remove last letter untel same of prefix
		for strings.Index(strs[i], prefix) != 0 {
			prefix = prefix[:len(prefix)-1]
			if prefix == "" {
				return ""
			}
		}
	}

	return prefix
}

// Test function
func main() {
	// Test cases
	tests := [][]string{
		{"flower", "flow", "flight"},
		{"dog", "racecar", "car"},
	}

	for _, test := range tests {
		fmt.Printf("Input: %v, Output: %s\n", test, longestCommonPrefix(test))
	}
}
